import React, { useEffect } from "react";
import { Routes, Route } from "react-router-dom";

import "./App.css";
import Footer from "./Footer";
import Header from "./Header";
import Products from "./pages/Products";
import Detail from "./pages/Detail";
import Cart from "./pages/Cart";
import Checkout from "./pages/Checkout";

export default function App() {
  return (
    <>
      <div className="content">
        <Header />
        <main>
          <Routes>
            <Route path="/">
              <h1>Welcome to Carved Rock fitness</h1>
            </Route>
            <Route path="/:category">
              <Products />
            </Route>
            <Route path="/:category/:id">
              <Detail />
            </Route>
            <Route path="/cart">
              <Cart />
            </Route>
            <Route path="/checkout">
              <Checkout />
            </Route>
          </Routes>
        </main>
      </div>
      <Footer />
    </>
  );
}
