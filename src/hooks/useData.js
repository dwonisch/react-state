import { useState, useEffect, useRef } from "react";
const baseUrl = process.env.REACT_APP_API_BASE_URL;

function useData(url) {
  const isMounted = useRef(false);
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    isMounted.current = true;
    async function fetchData() {
      try {
        setIsLoading(true);
        const response = await fetch(baseUrl + url);
        if (response.ok) {
          const d = await response.json();
          if (isMounted.current) setData(d);
        } else {
          throw response;
        }
      } catch (e) {
        if (isMounted.current) setError(e);
      } finally {
        if (isMounted.current) setIsLoading(false);
      }
    }
    fetchData();

    return () => {
      isMounted.current = false;
    };
  }, []);

  return { data, error, isLoading };
}

export default useData;
