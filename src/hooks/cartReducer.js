export default function cartReducer(cart, action) {
  const { id, sku, quantity } = action;

  switch (action.type) {
    case "empty":
      return [];
    case "add":
      const itemInCart = cart.find((i) => i.sku === sku);

      if (itemInCart) {
        const newItem = { ...itemInCart, quantity: itemInCart.quantity + 1 };

        return cart.map((i) => (i.id === newItem.id ? newItem : i));
      } else {
        return [...cart, { id, sku, quantity: 1 }];
      }
    case "update":
      return quantity === 0
        ? cart.filter((i) => i.sku != sku)
        : cart.map((i) => (i.sku === sku ? { ...i, quantity: quantity } : i));
    default:
      throw new Error("Action not implemented " + action.type);
  }
}
