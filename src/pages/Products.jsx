import React, {useState} from "react";
import { Link, useParams } from 'react-router-dom';

import Spinner from "../Spinner";
import PageNotFound from "../components/PageNotFound";

import useData from "../hooks/useData"

export default function Products() {
  const [size, setSize] = useState("");
  const { category } = useParams();

  const {data: products, error, isLoading} = useData(`products?category=${category}`);

  function renderProduct(p) {
    return (
      <div key={p.id} className="product">
        <Link to={`/${category}/${p.id}`}>
          <img src={`/images/${p.image}`} alt={p.name} />
          <h3>{p.name}</h3>
          <p>${p.price}</p>
        </Link>
      </div>
    );
  }

if(isLoading)
return <Spinner />

  if(!products.length){
    return <PageNotFound />
  }

  let filteredProducts = size ? products.filter(p => p.skus.find(s => s.size === parseInt(size))) : products;

  if(error)
    throw error;

  return (
            <>
            <h1>{category}</h1>
              <section id="filters">
                <label htmlFor="size">Filter by Size:</label>{" "}
                <select id="size" value={size} onChange={event => setSize(event.target.value)}>
                  <option value="">All sizes</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                </select>
                { size && <h2>Found {filteredProducts.length} items</h2>}
              </section>
              <section id="products">
                {filteredProducts.map(renderProduct)}
              </section>
            </>
  );
}
