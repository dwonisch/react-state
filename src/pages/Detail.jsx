import React, { useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import useData from "../hooks/useData";
import { useCart } from "../services/cartContext";

import Spinner from "../Spinner";
import PageNotFound from "../components/PageNotFound";

export default function Detail() {
  const navigate = useNavigate();
  const { category, id } = useParams();
  const { data: product, error, isLoading } = useData(`products/${id}`);
  const [sku, setSku] = useState("");
  const { dispatch } = useCart();

  if (isLoading) return <Spinner></Spinner>;

  if (Array.isArray(product)) return <PageNotFound />;
  if (error) throw error;

  return (
    <div id="detail">
      <h1>{product.name}</h1>
      <p>{product.description}</p>
      <img src={`/images/${product.image}`} alt={product.name} />
      <p>${product.price}</p>
      <select id="size" value={sku} onChange={(e) => setSku(e.target.value)}>
        <option value="">What size?</option>
        {product.skus.map((s) => (
          <option key={s.sku} value={s.sku}>
            {s.size}
          </option>
        ))}
      </select>
      <p>
        <button
          disabled={!sku}
          className="btn btn-primary"
          onClick={() => {
            dispatch({ type: "add", id, sku });
            navigate("/cart");
          }}
        >
          Add to cart
        </button>
      </p>
    </div>
  );
}
